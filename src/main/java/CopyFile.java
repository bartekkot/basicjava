import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by x on 5/18/17.
 */
public class CopyFile {

    public static void main(String[] args) {
        File source = new File(args[0]);
        ///home/x/Desktop/newFile.txt
        File dest = new File("/home/x/Desktop/newFile2.txt");
        try {
            copyFileUsingApacheCommonsIO(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private static void copyFileUsingApacheCommonsIO(File source, File dest) throws IOException {
        FileUtils.copyFile(source, dest);
    }
}
