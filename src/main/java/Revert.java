import java.util.Arrays;
import java.util.Collections;

/**
 * Created by x on 5/18/17.
 */
public class Revert {
    public static void main(String[] args) {

        Arrays.sort(args, Collections.reverseOrder());
        for(int i = 0; i < args.length; i++ ) {
            System.out.println(args[i]);
        }
    }
}
